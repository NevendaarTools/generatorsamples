﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using net.r_eg.DllExport;
//using net.r_eg.Conari.Types;
using System.Runtime.InteropServices;
using System.IO;
using SharpGenSample.MapObject;
using NevendaarTools;
using System.Reflection;
using RandomStackGenerator;

namespace SharpGenSample
{
    public class GeneratorImpl
    {
        private static GeneratorImpl instance = new GeneratorImpl();
        List<Option> options = new List<Option>();
        GameModel gameModel = new GameModel();
        Assembly assembly = null;

        private GeneratorImpl()
        {
            //var dllFile = new FileInfo(@"Generators\Newtonsoft.Json.dll");
            //assembly = Assembly.LoadFile(dllFile.FullName);
            //var dllDirectory = dllFile.Directory.FullName;
            //File.WriteAllText(@"C:\NevendaarTools\D2MapEditorQt\Generators\WriteText.txt", dllDirectory);
            //Environment.SetEnvironmentVariable("PATH", Environment.GetEnvironmentVariable("PATH") + ";" + dllDirectory);
            //AppDomain.CurrentDomain.AppendPrivatePath(dllDirectory);
            //AppDomain.CurrentDomain.AppendPrivatePath("Generators");
            //AppDomain.CurrentDomain.AppendPrivatePath("../Generators");
            //AppDomainSetup.PrivateBinPath += dllDirectory;
        }


        public static GeneratorImpl GetInstance()
        {
            return instance;
        }

        public void Init(string path, string lang)
        {
            gameModel.Load(path, false);
            options.Clear();
            options.Add(new Option() { name = "StackGen: ", value = GenDefaultValues.myVersion, type = (int)Option.Type.StringLabel });

            options.Add(new Option() { name = "MapSize", value = "48x48", variants = "48x48;72x72;96x96;144x144", type = (int)Option.Type.Enum });
            options.Add(new Option() { name = "Races:", value = "", type = (int)Option.Type.StringLabel });
            var races = gameModel.GetAllT<Grace>();
            foreach (var race in races)
            {
                options.Add(new Option() { name = race.name_txt.value.text, value = "False", type = (int)Option.Type.Bool });
            }
            options.Add(new Option() { name = "CapitalGuards", value = "False", type = (int)Option.Type.Bool });
        }

        public string getName()
        {
            return "SharpGenSample";
        }

        public string getDesc()
        {
            return "SharpGenSample desc";
        }

        

        public int getOptionsCount()
        {
            return options.Count;
        }

        public IntPtr getOptionAt(int num)
        {
            IntPtr ptr = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(Option)));
            Marshal.StructureToPtr(options[num], ptr, false);
            return ptr;
        }

        public int setOptionAt(int index, string value)
        {
            Option opt = options[index];
            opt.value = value;
            options[index] = opt;
            return 0;
        }
        
        public void AddCapital(ref JsonMap map, string race, string guardId, string heroId, int x, int y)
        {
            CapitalObject capital = new CapitalObject();
            capital.raceId = race;
            capital.name = race + "_capital";
            capital.desc = "desc";
            capital.x = x;
            capital.y = y;
            if (guardId != null)
            {
                Unit guard = new Unit();
                guard.id = guardId;
                capital.garrison.AddUnit(guard, new MapObject.Point(0, 1));
            }
            Unit hero = new Unit();
            hero.id = heroId;
            capital.visiter.AddUnit(hero, new MapObject.Point(0, 1));
            map.AddObject(capital);

            CrystalObject crystal = new CrystalObject();
            crystal.x = x + 2;
            crystal.y = y + 7;
            map.AddObject(crystal);

            LandmarkObject landmark = new LandmarkObject();
            landmark.x = x + 2;
            landmark.y = y + 10;
            landmark.desc = "desc";
            landmark.lmarkId = "G000MG0004";
            map.AddObject(landmark);

            LocationObject location =  new LocationObject();
            location.x = x + 1;
            location.y = y + 9;
            location.name = race;
            location.r = 3;
            map.AddObject(location);

            MerchantObject merchant = new MerchantObject();
            merchant.desc = "desc";
            merchant.name = race;
            merchant.type = MerchantObject.MerchantType.Items;
            merchant.inventory.AddItem("g000ig0001", 5);
            merchant.x = x + 1;
            merchant.y = y + 13;
            map.AddObject(merchant);

            MountainObject mountain = new MountainObject();
            mountain.x = x + 1;
            mountain.y = y + 17;
            mountain.w = 2;
            mountain.h = 2;
            map.AddObject(mountain);

            RuinObject ruin = new RuinObject();
            ruin.x = x + 1;
            ruin.y = y + 20;
            ruin.reward.gold = 100;
            map.AddObject(ruin);

            StackObject stack = new StackObject(x + 2, y + 25);
            stack.stack.AddUnit(hero, new MapObject.Point(1, 1));
            stack.stack.units[0].key.leader = true;
            stack.stack.name = "stack_" + race;
            map.AddObject(stack);

            TreasureObject treasure = new TreasureObject();
            treasure.x = x + 1;
            treasure.y = y + 26;
            treasure.inventory.AddItem("g000ig0001", 2);
            map.AddObject(treasure);

            VillageObject villageObject = new VillageObject();
            villageObject.x = x + 1;
            villageObject.y = y + 28;
            villageObject.level = 1;
            villageObject.name = "village name";
            map.AddObject(villageObject);
        }

        public string generateMap()
        {
            JsonMap map = new JsonMap();
            var sizeOpt = Option.GetOption(ref options, "MapSize");
            Console.WriteLine(sizeOpt);
            int size = 48;
            var sizeValues = sizeOpt.Split('x');
            if (sizeValues.Length > 0)
                size = int.Parse(sizeValues[0]);
            map.grid.init(size);
            int posX = 1;
            int posY = 1;
            int capCount = 0;
            var races = gameModel.GetAllT<Grace>();
            bool enabledGuard = Option.GetBoolOption(ref options, "CapitalGuards"); ;
            foreach (var race in races)
            {
                bool enabled = Option.GetBoolOption(ref options, race.name_txt.value.text);
                if (enabled)
                {
                    string guardId = null;
                    if (enabledGuard)
                        guardId = race.guardian.value.unit_id;
                    AddCapital(ref map, race.race_id, guardId, race.leader_1.value.unit_id, posX, posY);
                    posX += 6;
                    capCount++;
                }
            }
            string result= map.ToString();
            File.WriteAllText(@"C:\NevendaarTools\D2MapEditorQt\Generators\WriteText.json", result);
            return result;
        }

        public void cleanup()
        {

        }
    }
}

